/// <reference types="cypress" />

const resultsList = [
  {
    premiumRooms: 3,
    economyRooms: 3,
    premiumPrice: 738,
    economyPrice: 167,
    occupiedPremiumRooms: 3,
    occupiedEconomyRooms: 3,
  },
  {
    premiumRooms: 7,
    economyRooms: 5,
    premiumPrice: 1054,
    economyPrice: 189,
    occupiedPremiumRooms: 6,
    occupiedEconomyRooms: 4,
  },
  {
    premiumRooms: 2,
    economyRooms: 7,
    premiumPrice: 583,
    economyPrice: 189,
    occupiedPremiumRooms: 2,
    occupiedEconomyRooms: 4,
  },
  {
    premiumRooms: 7,
    economyRooms: 1,
    premiumPrice: 1153,
    economyPrice: 45,
    occupiedPremiumRooms: 7,
    occupiedEconomyRooms: 1,
  },
]

describe("Test app flow", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/")
  })

  it("should calculate the appropriate price for the different options", () => {
    resultsList.map(
      ({
        premiumRooms,
        economyRooms,
        premiumPrice,
        economyPrice,
        occupiedPremiumRooms,
        occupiedEconomyRooms,
      }) =>
        checkPrice(
          premiumRooms,
          economyRooms,
          premiumPrice,
          economyPrice,
          occupiedPremiumRooms,
          occupiedEconomyRooms,
        ),
    )
  })
})

const checkPrice = (
  premiumRooms,
  economyRooms,
  premiumPrice,
  economyPrice,
  occupiedPremiumRooms,
  occupiedEconomyRooms,
) => {
  cy.get("[data-cy='input']").eq(0).clear().type(premiumRooms)
  cy.get("[data-cy='input']").eq(1).clear().type(economyRooms)

  cy.contains("Calculate").click()

  cy.get("[data-cy='result']").should(
    "contain",
    `Occupied Premium Rooms - ${occupiedPremiumRooms}`,
  )
  cy.get("[data-cy='result']").should(
    "contain",
    `Occupied Economy Rooms - ${occupiedEconomyRooms}`,
  )
  cy.get("[data-cy='result']").should("contain", `Price: ${premiumPrice} €`)
  cy.get("[data-cy='result']").should("contain", `Price: ${economyPrice} €`)

  cy.wait(1000)
}

## Available Scripts

In the project directory, you can run:

### `npm start`

Before starting the application, you must run the `npm install` command to install all dependencies.\
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run cypress`

Launches the Cypress tests. You must start the project with `npm start` to make them work.

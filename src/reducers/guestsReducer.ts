import {
  ERROR_RECEIVE_GUESTS_DATA,
  RECEIVE_GUESTS_DATA,
  REQUEST_GUESTS_DATA,
} from "../constants/constants"

interface guestsReducerProps {
  guestsData: number[]
  isLoading: boolean
  isError: boolean
}

const initialState: guestsReducerProps = {
  guestsData: [],
  isLoading: false,
  isError: false,
}

const guestsReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_GUESTS_DATA:
      return {
        ...state,
        isLoading: true,
        isError: false,
      }
    case RECEIVE_GUESTS_DATA:
      return {
        ...state,
        guestsData: action.guestsData,
        isLoading: false,
      }
    case ERROR_RECEIVE_GUESTS_DATA:
      return {
        ...state,
        guestsData: [],
        isLoading: false,
        isError: true,
      }
    default:
      return state
  }
}

export default guestsReducer

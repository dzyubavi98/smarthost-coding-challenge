import { CALCULATE_OCCUPIED_ROOMS_AND_PRICE } from "../constants/constants"

interface roomsReducerProps {
  isCalculated: boolean
  premiumRoomsPrice: number
  economyRoomsPrice: number
  premiumRoomsOccupied: number
  economyRoomsOccupied: number
}

const initialState: roomsReducerProps = {
  isCalculated: false,
  premiumRoomsPrice: 0,
  economyRoomsPrice: 0,
  premiumRoomsOccupied: 0,
  economyRoomsOccupied: 0,
}

const roomsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CALCULATE_OCCUPIED_ROOMS_AND_PRICE:
      return {
        ...state,
        isCalculated: true,
        premiumRoomsPrice: action.premiumRoomsPrice,
        economyRoomsPrice: action.economyRoomsPrice,
        premiumRoomsOccupied: action.premiumRoomsOccupied,
        economyRoomsOccupied: action.economyRoomsOccupied,
      }
    default:
      return state
  }
}

export default roomsReducer

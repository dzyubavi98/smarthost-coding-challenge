import React from "react"
import styled from "styled-components"

interface InputProps {
  label?: string
  onChange: (e) => void
  error?: boolean
}

export const Input = ({ onChange, label, error = false }: InputProps) => {
  return (
    <div style={{ width: "100%" }}>
      <Label>{label}</Label>
      <InputWrapper
        data-cy="input"
        type="number"
        placeholder="Type rooms"
        onChange={onChange}
        error={error}
      />
      {error && <ErrorLabel>Please enter the correct value</ErrorLabel>}
    </div>
  )
}

const Label = styled.p`
  font-size: 15px;
  color: #666666;
  margin: 0 0 8px 0;
`

const ErrorLabel = styled(Label)`
  color: #e84034;
  margin: 8px 0px 0px 8px;
`

const InputWrapper = styled.input<{ error: boolean }>`
  width: -webkit-fill-available;
  padding: 14px;
  border-radius: 4px;
  border: ${({ error }) => (error ? "1px solid #E84034" : "1px solid #b3b3b3")};
  font-size: 15px;
  line-height: 22px;

  :hover {
    border-color: ${({ error }) => (error ? "#E84034" : "#cfeafa")};
  }

  :focus-visible {
    border-color: ${({ error }) => (error ? "#E84034" : "#1a87cc")};
    outline: none;
  }

  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`

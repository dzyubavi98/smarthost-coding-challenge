import React from "react"
import styled from "styled-components"

interface ResultProps {
  label: string
  roomsAvailable: string
  roomsPrice: string
}

export const Result = ({ label, roomsAvailable, roomsPrice }: ResultProps) => {
  return (
    <div data-cy="result">
      <Label>
        {label} - {roomsAvailable}
      </Label>
      <H4>Price: {roomsPrice} &#x20AC;</H4>
    </div>
  )
}

const H4 = styled.h4`
  margin: 0;
  font-size: 32px;
`

const Label = styled.p`
  font-size: 15px;
  color: #666666;
  margin: 0 0 8px 0;
`

import React from "react"
import styled from "styled-components"

export const Button = ({ onClick }) => {
  return <ButtonWrapper onClick={onClick}>Calculate</ButtonWrapper>
}

const ButtonWrapper = styled.button`
  width: 100%;
  border: 2px transparent solid;
  background-color: #1a87cc;
  color: #ffffff;
  border-radius: 4px;
  font-size: 15px;
  line-height: 22px;
  font-weight: 700;
  padding: 11px 18px;
  cursor: pointer;
  overflow: hidden;
  transform: translate3d(0, 0, 0);

  :hover {
    border: 2px solid rgba(0, 0, 0, 0.04);
    background-color: #1a82c4;
  }

  &:after {
    content: "";
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    pointer-events: none;
    background-image: radial-gradient(circle, #000 10%, transparent 10.01%);
    background-repeat: no-repeat;
    background-position: 50%;
    transform: scale(10, 10);
    opacity: 0;
    transition: transform 0.5s, opacity 1s;
  }

  &:active:after {
    transform: scale(0, 0);
    opacity: 0.2;
    transition: 0s;
  }
`

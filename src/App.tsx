import React, { useEffect, useState } from "react"
import styled from "styled-components"
import { calculateRooms, requestGuests } from "./actions/actions"
import { useAppDispatch, useAppSelector } from "./hooks/hooks"
import { Button } from "./components/Button"
import { Input } from "./components/Input"
import { Result } from "components/Result"
import { calculateRoomsAndPrice } from "./helpers/calculateRoomsAndPrice"

const App = () => {
  const { guestsData } = useAppSelector((state) => state.guestsReducer)
  const {
    isCalculated,
    premiumRoomsPrice,
    economyRoomsPrice,
    premiumRoomsOccupied,
    economyRoomsOccupied,
  } = useAppSelector((state) => state.roomsReducer)
  const dispatch = useAppDispatch()

  const [premiumRooms, setPremiumRooms] = useState(-1)
  const [economyRooms, setEconomyRooms] = useState(-1)
  const [error, setError] = useState(false)

  useEffect(() => {
    dispatch(requestGuests())
  }, [])

  const onClickHandler = () => {
    const {
      premiumRoomsPrice,
      economyRoomsPrice,
      premiumRoomsOccupied,
      economyRoomsOccupied,
    } = calculateRoomsAndPrice({ guestsData, premiumRooms, economyRooms })

    if (premiumRooms >= 0 && economyRooms >= 0) {
      setError(false)
      dispatch(
        calculateRooms({
          premiumRoomsPrice,
          economyRoomsPrice,
          premiumRoomsOccupied,
          economyRoomsOccupied,
        }),
      )
    } else {
      setError(true)
    }
  }

  return (
    <AppLayout>
      <Modal>
        <InputsWrapper>
          <Input
            label="Premium Rooms"
            onChange={({ target }) => {
              setPremiumRooms(parseInt(target.value))
              setError(false)
            }}
            error={error && !(premiumRooms >= 0)}
          />
          <Input
            label="Economy Rooms"
            onChange={({ target }) => {
              setEconomyRooms(parseInt(target.value))
              setError(false)
            }}
            error={error && !(economyRooms >= 0)}
          />
          <Button onClick={onClickHandler} />
        </InputsWrapper>
        {isCalculated && (
          <ResultsWrapper>
            <Result
              label="Occupied Premium Rooms"
              roomsAvailable={premiumRoomsOccupied}
              roomsPrice={premiumRoomsPrice}
            />
            <Result
              label="Occupied Economy Rooms"
              roomsAvailable={economyRoomsOccupied}
              roomsPrice={economyRoomsPrice}
            />
          </ResultsWrapper>
        )}
      </Modal>
    </AppLayout>
  )
}

export default App

const ResultsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 40px;
`

const InputsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 28px;
  flex-wrap: wrap;
`

const AppLayout = styled.div`
  height: 100vh;
  background-color: #cfeafa;
`

const Modal = styled.div`
  position: absolute;
  display: flex;
  width: 50%;
  max-width: 500px;
  top: 50%;
  left: 50%;
  padding: 40px;
  flex-direction: column;
  transform: translate(-50%, -50%);
  background-color: white;
  border-radius: 14px;
  box-shadow: 0px 6px 12px rgba(0, 0, 0, 0.12);
`

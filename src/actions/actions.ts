import axios from "axios"
import {
  CALCULATE_OCCUPIED_ROOMS_AND_PRICE,
  ERROR_RECEIVE_GUESTS_DATA,
  RECEIVE_GUESTS_DATA,
  REQUEST_GUESTS_DATA,
} from "../constants/constants"

export const requestGuests = () => {
  return async function (dispatch) {
    dispatch({
      type: REQUEST_GUESTS_DATA,
    })
    try {
      const json = await axios.get("guests.json")

      dispatch({
        type: RECEIVE_GUESTS_DATA,
        guestsData: json.data,
      })
    } catch (e) {
      dispatch({
        type: ERROR_RECEIVE_GUESTS_DATA,
      })
    }
  }
}

export const calculateRooms = (data) => {
  return function (dispatch) {
    dispatch({
      type: CALCULATE_OCCUPIED_ROOMS_AND_PRICE,
      premiumRoomsPrice: data.premiumRoomsPrice,
      economyRoomsPrice: data.economyRoomsPrice,
      premiumRoomsOccupied: data.premiumRoomsOccupied,
      economyRoomsOccupied: data.economyRoomsOccupied,
    })
  }
}

export const calculateRoomsAndPrice = ({
  guestsData,
  premiumRooms,
  economyRooms,
}) => {
  const premiumGuests = guestsData
    .filter((moneyWillingToPay) => moneyWillingToPay >= 100)
    .sort((a, b) => {
      return b - a
    })

  const economyGuests = guestsData
    .filter((moneyWillingToPay) => moneyWillingToPay < 100)
    .sort((a, b) => {
      return b - a
    })

  const calculateTotalValue = (array) => array.reduce((a, b) => a + b, 0)
  const requiredEconomyRooms = economyGuests.length - economyRooms
  const freePremiumRooms = premiumRooms - premiumGuests.length

  if (freePremiumRooms > 0 && requiredEconomyRooms > 0) {
    const economyRoomsPrice = economyGuests.slice(0, requiredEconomyRooms)

    const premiumRoomsAvailable = premiumGuests
      .concat(economyRoomsPrice)
      .slice(0, premiumRooms)

    const economyRoomsAvailable = economyGuests
      .filter((val) => !premiumRoomsAvailable.includes(val))
      .slice(0, economyRooms)

    return {
      premiumRoomsPrice: calculateTotalValue(premiumRoomsAvailable),
      economyRoomsPrice: calculateTotalValue(economyRoomsAvailable),
      premiumRoomsOccupied: premiumRoomsAvailable.length,
      economyRoomsOccupied: economyRoomsAvailable.length,
    }
  } else {
    const premiumRoomsAvailable = premiumGuests.slice(0, premiumRooms)
    const economyRoomsAvailable = economyGuests.slice(0, economyRooms)

    return {
      premiumRoomsPrice: calculateTotalValue(premiumRoomsAvailable),
      economyRoomsPrice: calculateTotalValue(economyRoomsAvailable),
      premiumRoomsOccupied: premiumRoomsAvailable.length,
      economyRoomsOccupied: economyRoomsAvailable.length,
    }
  }
}

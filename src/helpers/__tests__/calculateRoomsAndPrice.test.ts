import { calculateRoomsAndPrice } from "../calculateRoomsAndPrice"

describe("calculateRoomsAndPrice", () => {
  const guestsData = [23, 45, 155, 374, 22, 99, 100, 101, 115, 209]

  const resultsList = [
    {
      premiumRooms: 3,
      economyRooms: 3,
      premiumPrice: 738,
      economyPrice: 167,
      occupiedPremiumRooms: 3,
      occupiedEconomyRooms: 3,
    },
    {
      premiumRooms: 7,
      economyRooms: 5,
      premiumPrice: 1054,
      economyPrice: 189,
      occupiedPremiumRooms: 6,
      occupiedEconomyRooms: 4,
    },
    {
      premiumRooms: 2,
      economyRooms: 7,
      premiumPrice: 583,
      economyPrice: 189,
      occupiedPremiumRooms: 2,
      occupiedEconomyRooms: 4,
    },
    {
      premiumRooms: 7,
      economyRooms: 1,
      premiumPrice: 1153,
      economyPrice: 45,
      occupiedPremiumRooms: 7,
      occupiedEconomyRooms: 1,
    },
  ]
  test.each(resultsList)(
    `should return proper, if premium rooms = $premiumRooms, economy rooms = $economyRooms, `,
    ({
      premiumRooms,
      economyRooms,
      premiumPrice,
      economyPrice,
      occupiedPremiumRooms,
      occupiedEconomyRooms,
    }) => {
      const result = calculateRoomsAndPrice({
        guestsData: guestsData,
        premiumRooms: premiumRooms,
        economyRooms: economyRooms,
      })

      expect(result).toStrictEqual({
        premiumRoomsPrice: premiumPrice,
        economyRoomsPrice: economyPrice,
        premiumRoomsOccupied: occupiedPremiumRooms,
        economyRoomsOccupied: occupiedEconomyRooms,
      })
    },
  )
})
